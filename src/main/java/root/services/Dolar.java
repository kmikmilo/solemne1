/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;
//FALTA ARREGLARLO

import java.io.StringReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;

/**
 *
 * @author PC
 */
@Path("/")
public class Dolar {

    String apiKey = "3c63675e1c7ab638de6d3f1f1393bc32c20b64d7";

    @GET
    @Path("/dolar/{fecha}")
    @Produces({MediaType.TEXT_HTML, MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})

    public String getDolar(@PathParam("fecha") String date) {

        Client client = ClientBuilder.newClient();

        String fech[] = date.split("-");

        String url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/" + fech[0] + "/" + fech[1] + "/dias/" + fech[2] + "/?formato=JSON&apikey=" + apiKey;

        Response respuestaSBIF = client.target(url).request().get();

        String datoRespuestaSBIF = respuestaSBIF.readEntity(String.class);

        JsonObject j = Json.createReader(new StringReader(datoRespuestaSBIF)).readObject();
        JsonArray valorDolar = j.getJsonArray("Dolares");
        //  JsonObject jsonExtraido = valorDolar.getJsonObject(0);
        JsonArray Fecha = j.getJsonArray("Dolares");
        //String valor = j.getString("Valor");
        JsonObject respuesta = Json.createObjectBuilder()
                .add("mensaje", "Esta fecha no tiene datos por ser fin de semana o festivo, pero se mostraran los ultimos datos validos")
                .build();
        JsonArray mensaje = j.getJsonArray("mensaje");

        while (Fecha == null) {
            int entero;
            entero = Integer.parseInt(fech[2]);
            entero = entero - 1;
            System.out.println(fech[2]);
            fech[2] = Integer.toString(entero);
            client = ClientBuilder.newClient();
            url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/" + fech[0] + "/" + fech[1] + "/dias/" + fech[2] + "/?formato=JSON&apikey=" + apiKey;

            respuestaSBIF = client.target(url).request().get();

            datoRespuestaSBIF = respuestaSBIF.readEntity(String.class);
            j = Json.createReader(new StringReader(datoRespuestaSBIF)).readObject();
            valorDolar = j.getJsonArray("Dolares");
            //  JsonObject jsonExtraido = valorDolar.getJsonObject(0);
            Fecha = j.getJsonArray("Dolares");
            System.out.println(fech[2]);
            // JsonObject jsonExtraido = Fecha.getJsonObject(0);
            //String valor = jsonExtraido.getString("Valor");
            System.out.println(Fecha);
        }

        JsonObject jr = Json.createObjectBuilder()
                // .add("valor del dolar",valor)
                //.add("fecha de la consulta", Fecha)
                .add("Mensaje", "Estos datos son obtenidos de la pagina de SBIF")
                .build();
        JsonObject jsonExtraido = Fecha.getJsonObject(0);
        String valor = jsonExtraido.getString("Valor");

        System.out.println(valor);

        return jr.toString() + mensaje + datoRespuestaSBIF + Fecha;
    }

    public String dolarHoy(String date) {
        Client client = ClientBuilder.newClient();

        LocalDate date2 = LocalDate.now();
        String sDate2 = date2.format(DateTimeFormatter.ofPattern("yyyy-mm-dd"));
        String fecha[] = sDate2.split("-");

        String url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/" + fecha[0] + "/" + fecha[1] + "/dias/" + fecha[2] + "/?formato=JSON&apikey=" + apiKey;

        Response respuestaSBIF = client.target(url).request().get();

        String datoRespuestaSBIF = respuestaSBIF.readEntity(String.class);

        JsonObject j = Json.createReader(new StringReader(datoRespuestaSBIF)).readObject();
        JsonArray valorDolar = j.getJsonArray("Dolares");
        JsonObject jsonExtraido = valorDolar.getJsonObject(0);
        String Fecha = jsonExtraido.getString("Fecha");
        String valor = jsonExtraido.getString("Valor");

        JsonObject jr = Json.createObjectBuilder()
                .add("valor del dolar", valor)
                .add("fecha de la consulta", Fecha)
                .add("Mensaje", "Estos datos son obtenidos de la pagina de SBIF")
                .build();

        return jr.toString();

    }

    public String hoy() {
        Client client = ClientBuilder.newClient();

        LocalDate date2 = LocalDate.now();
        String sDate2 = date2.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String fecha[] = sDate2.split("-");

        String url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/" + fecha[0] + "/" + fecha[1] + "/dias/" + fecha[2] + "/?formato=JSON&apikey=" + apiKey;

        Response respuestaSBIF = client.target(url).request().get();

        String datoRespuestaSBIF = respuestaSBIF.readEntity(String.class);

        JsonObject j = Json.createReader(new StringReader(datoRespuestaSBIF)).readObject();
        JsonArray Fecha = j.getJsonArray("Dolares");
        String valor = "0";

        while (Fecha == null) {
            int entero;
            entero = Integer.parseInt(fecha[2]);
            entero = entero - 1;

            fecha[2] = Integer.toString(entero);
            client = ClientBuilder.newClient();
            url = "https://api.sbif.cl/api-sbifv3/recursos_api/dolar/" + fecha[0] + "/" + fecha[1] + "/dias/" + fecha[2] + "/?formato=JSON&apikey=" + apiKey;

            respuestaSBIF = client.target(url).request().get();

            datoRespuestaSBIF = respuestaSBIF.readEntity(String.class);
            j = Json.createReader(new StringReader(datoRespuestaSBIF)).readObject();

            Fecha = j.getJsonArray("Dolares");

        }
        JsonObject jsonExtraido = Fecha.getJsonObject(0);
        valor = jsonExtraido.getString("Valor");

        return valor;
    }

}
